from django.http.response import Http404
from django.shortcuts import render


def beek_test(request):
    return render(request, 'survey/beek_test.html')


def new_comers(request):
    return render(request, 'survey/new_comers.html')


def ajax_post(request):
    if request.is_ajax() and request.POST:
        data = request.POST.get('item')
        print(data)
    else:
        return Http404()
