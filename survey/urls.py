from django.conf import settings
from django.urls import path

from .views import beek_test, new_comers, ajax_post
app_name = 'survey'

urlpatterns = [

    path('beek-depression-test/', beek_test, name="beek_test"),
    path('new-comers/', new_comers, name="new_comers"),
    path('ajax/post', ajax_post)

]
