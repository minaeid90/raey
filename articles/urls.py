from django.conf import settings
from django.urls import path

from .views import ArticleListView, ArticleDetailView, articles_of_category
app_name = 'articles'
urlpatterns = [

    path('', ArticleListView.as_view(), name="articles_list"),
    path('<int:pk>/', ArticleDetailView.as_view(), name="article_detail"),
    path('category/<int:pk>/', articles_of_category, name="articles_category")
    # path('ckeditor/', include('ckeditor_uploader.urls')),
]
