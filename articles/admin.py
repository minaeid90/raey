from django.apps import AppConfig
from django.contrib import admin
from django.contrib.admin import register
from django.utils.translation import gettext_lazy as _
from image_cropping import ImageCroppingMixin

from .models import Article, Author, Category, Issue
from .forms import ArticleForm

admin.site.index_title = _('My Index Title')
admin.site.site_header = _('My Site Administration')
admin.site.site_title = _('My Site Management')


@admin.register(Article)
class ArticleAdmin(ImageCroppingMixin, admin.ModelAdmin):

    form = ArticleForm

    list_display = ('title', 'category')

    list_filter = ('issue', 'category')
    # inlines = [
    #     Inline,
    # ]
    # raw_id_fields = ('',)
    readonly_fields = ('get_words_count',)
    search_fields = ('title', 'content',)
    # date_hierarchy = ''
    # ordering = ('',)

    def get_words_count(self, obj):
        return len(obj.content)


@admin.register(Author)
class AuthorAdmin(ImageCroppingMixin, admin.ModelAdmin):

    list_display = ('name',)

    # list_filter = ('',)
    # inlines = [
    #     Inline,
    # ]
    # raw_id_fields = ('',)
    # readonly_fields = ('',)
    # search_fields = ('',)
    # date_hierarchy = ''
    # ordering = ('',)


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):

    list_display = ('name', 'order')
    list_editable = ('order',)
    list_filter = ('name',)
    # inlines = [
    #     Inline,
    # ]
    # raw_id_fields = ('',)
    # readonly_fields = ('',)
    search_fields = ('name',)
    # date_hierarchy = ''
    ordering = ('order',)


@admin.register(Issue)
class IssueAdmin(admin.ModelAdmin):

    list_display = ('name', 'number', 'pub_date')
    # list_editable = ('order',)
    list_filter = ('name',)
    # inlines = [
    #     Inline,
    # ]
    # raw_id_fields = ('',)
    # readonly_fields = ('',)
    search_fields = ('name', 'number')
    # date_hierarchy = ''
    ordering = ('number',)
