from django import forms
from django.core.exceptions import ValidationError
from django.utils.text import format_lazy
from django.utils.translation import gettext_lazy as _

from .models import Article


class ArticleForm(forms.ModelForm):

    class Meta:
        model = Article
        exclude = ()

    def clean(self):

        content = self.cleaned_data.get('content')
        category = self.cleaned_data.get('category')
        words_count = len(content.split())
        cnt_l = _("Content must be at least")
        cnt_g = _("Content must be not greater than")
        p_cnt = _("word")
        min_msg = format_lazy("{cnt_l} {val} {p_cnt}", cnt_l=cnt_l,
                              val=category.min_words, p_cnt=p_cnt)
        max_msg = format_lazy("{cnt_g} {val} {p_cnt}", cnt_g=cnt_g,
                              val=category.max_words, p_cnt=p_cnt)
        if words_count < category.min_words:
            raise ValidationError(min_msg)
        if words_count > category.max_words:
            raise ValidationError(max_msg)
        return self.cleaned_data
