import datetime

from autoslug import AutoSlugField
from ckeditor.fields import RichTextField
from django.db import models
from django.urls import reverse
from django.utils.translation import gettext_lazy as _
from image_cropping import ImageRatioField
from taggit.managers import TaggableManager


class Issue(models.Model):
    def pdf_file_path(instance, filename):
        return 'pdfs/issue_{0}.{1}'.format(instance.number, filename.split(".")[1])
    name = models.CharField(max_length=255, verbose_name=_('Name'))
    number = models.IntegerField(unique=True, verbose_name=_('Number'))
    pdf_file = models.FileField(
        upload_to=pdf_file_path, max_length=100,
        blank=True, null=True, verbose_name=_('PDF file'))
    is_published = models.BooleanField(
        default=False, verbose_name=_('Is Published'))
    pub_date = models.DateField(
        null=True, blank=True, verbose_name=_('Publication date'))

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = _('Issue')
        verbose_name_plural = _('Issues')
        ordering = ('number',)

    def __str__(self):
        return self.name


class Category(models.Model):
    WRITING_CHOICES = [
        ('0', 'كتابة'),
        ('1', 'إعداد'),
        ('2', 'حوار'),
        ('3', 'قلم'),
    ]
    name = models.CharField(max_length=50, verbose_name=_('Name'))
    order = models.SmallIntegerField(default=0, verbose_name=_('Order'))
    writing_type = models.CharField(
        max_length=1, choices=WRITING_CHOICES,
        default="0", verbose_name=_('Writing type')
    )
    min_words = models.SmallIntegerField(
        default=0, verbose_name=_("Min words"))
    max_words = models.SmallIntegerField(
        default=0, verbose_name=_("Max words"))
    show_author_image = models.BooleanField(
        default=False, verbose_name=_('Show author image'))
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = _('Category')
        verbose_name_plural = _('Categories')
        ordering = ['order']

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('articles:articles_category', kwargs={'pk': self.pk})


class Author(models.Model):
    def author_image_path(instance, filename):
        return 'images/authors/{0}.{1}'.format(instance.pk, filename.split(".")[1])

    name = models.CharField(max_length=50, verbose_name=_('Name'))
    image = models.ImageField(
        upload_to=author_image_path, blank=True,
        null=True, verbose_name=_('Image'))
    cropping = ImageRatioField(
        'image', '430x360', verbose_name=_('Cropping Image'))

    class Meta:
        verbose_name = _('Author')
        verbose_name_plural = _('Authors')
        ordering = ['name']

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('articles:articles_category', kwargs={'pk': self.pk})


class Article(models.Model):
    PRIORITY = (
        (0, _('Low')),
        (1, _('Medium')),
        (2, _('High')),
    )

    def article_image_path(instance, filename):
        return 'images/articles/{0}.{1}'.format(instance.pk, filename.split(".")[1])

    title = models.CharField(max_length=255, verbose_name=_('Title'))
    slug = AutoSlugField(populate_from='title',
                         allow_unicode=True, verbose_name=_('Slug'))
    image = models.ImageField(
        upload_to=article_image_path, verbose_name=_('Image'))
    cropping = ImageRatioField('image', '430x360',  verbose_name=_('Cropping'))
    content = RichTextField(verbose_name=_('Content'),)
    main_article = models.BooleanField(
        default=False, verbose_name=_('Main article'))
    issue = models.ForeignKey(
        Issue, on_delete=models.CASCADE,  verbose_name=_('Issue'))
    category = models.ForeignKey(
        Category, on_delete=models.CASCADE, verbose_name=_('Category'))
    authors = models.ManyToManyField(Author, verbose_name=_('Authors'))
    priority = models.SmallIntegerField(
        choices=PRIORITY, default=0, verbose_name=_('Priority'))
    pub_date = models.DateField(
        default=datetime.date.today, verbose_name=_('Publication date'))
    tags = TaggableManager(verbose_name=_('Tags'))
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = _('Article')
        verbose_name_plural = _('Articles')

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('articles:article_detail', kwargs={'pk': self.pk})
