from django import template
from django.shortcuts import get_object_or_404
from django.urls import reverse
from .. import models


register = template.Library()


def grouped(l, n):
    # Yield successive n-sized chunks from l.
    for i in range(0, len(l), n):
        yield l[i:i + n]


@register.filter(name='getcat')
def get_category(value):
    obj = get_object_or_404(models.Category, pk=int(value))
    # print(obj)
    return reverse('articles:articles_category', kwargs={'pk': obj.pk})
    # return "yes"


@register.filter(name='concat')
def concat(value, arg):
    
    # print(obj)
    return value + arg
    # return "yes"


@register.filter
def group_by(value, arg):
    return grouped(value, arg)
