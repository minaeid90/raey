from django.db.models import Q
from django.shortcuts import get_object_or_404, render
from django.views.generic import DetailView, ListView
from meta.views import Meta


from .models import Article, Category, Issue


def index(request):
     # Get Last Issue
    last_issue = Issue.objects.filter(
        is_published=True).order_by('-pub_date').first()
    issue_number = last_issue.number
    print("Last issue is: {}".format(issue_number))
    # Get News
    qs_news = Article.objects.filter(
        category__name="أخبار").order_by('-priority', '-pub_date')
    news = qs_news.exclude(priority=2)[:4]
    main_new = qs_news.first()
    # Get Reports
    reports = Article.objects.filter(
        category__name="تقارير", issue__number=issue_number).order_by('-priority', '-pub_date')

    # Get Investigations
    investigations = Article.objects.filter(
        category__name="تحقيقات", issue__number=issue_number).order_by('-priority', '-pub_date')

    # Get Spirtuals
    spirtuals = Article.objects.filter(
        category__name="روحيات", issue__number=issue_number).order_by('-priority', '-pub_date')

    # Get Articles
    articles = Article.objects.filter(
        category__name="مقالات", issue__number=issue_number).exclude(main_article=True).order_by('-priority', '-pub_date')
    # Get Main Article
    main_article = Article.objects.filter(
        category__name="مقالات",  issue__number=issue_number, main_article=True).order_by('-pub_date').first()

    # Get Reportage
    reportage = Article.objects.filter(
        category__name="ريبورتاجات", issue__number=issue_number).order_by('-pub_date').first()

    context = {
        'm': main_new,
        'news': news,
        'reports': reports,
        'investigations': investigations,
        'spirtuals': spirtuals,
        'articles': articles,
        'main_article': main_article,
        'author_article': main_article.authors.first(),
        'reportage': reportage

    }

    return render(request, 'index.html', context)


def articles_of_category(request, pk, template='articles/entry_list.html',
                         page_template='articles/entry_list_page.html'):
    category = get_object_or_404(Category, pk=pk)
    articles = Article.objects.filter(
        category_id=category.id).order_by('-pub_date')
    context = {
        'category': category,
        'articles': articles,
        'page_template': page_template
    }
    if request.is_ajax():
        template = page_template
    return render(request, template, context)


class ArticleListView(ListView):
    model = Article
    template_name = "articles/articles_list.html"


class ArticleDetailView(DetailView):
    model = Article
    template_name = "articles/article_detail.html"
