from .models import Category, Issue
from django.db.models import Q

def add_variable_to_context(request):
    categories = Category.objects.all()
    last_issue = Issue.objects.filter(
        ~ Q(pdf_file=None)).order_by('-created_at').first()
    return {
        'categories': categories,
        'last_issue':last_issue
    }
