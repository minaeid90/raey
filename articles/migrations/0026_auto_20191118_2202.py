# Generated by Django 2.2 on 2019-11-18 20:02

import articles.models
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('articles', '0025_auto_20191118_2142'),
    ]

    operations = [
        migrations.AddField(
            model_name='issue',
            name='pdf_file',
            field=models.FileField(blank=True, null=True, upload_to=articles.models.Issue.pdf_file_path),
        ),
        migrations.AlterField(
            model_name='author',
            name='image',
            field=models.ImageField(blank=True, null=True, upload_to=articles.models.Author.author_image_path),
        ),
    ]
