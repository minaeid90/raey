# Generated by Django 2.2 on 2019-12-09 22:26

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('articles', '0033_auto_20191205_1822'),
    ]

    operations = [
        migrations.AlterField(
            model_name='article',
            name='authors',
            field=models.ManyToManyField(to='articles.Author', verbose_name='Authors'),
        ),
        migrations.AlterField(
            model_name='article',
            name='category',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='articles.Category', verbose_name='Category'),
        ),
        migrations.AlterField(
            model_name='article',
            name='issue',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='articles.Issue', verbose_name='Issue'),
        ),
    ]
