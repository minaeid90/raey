from django.conf import settings
from django.conf.urls import include
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path

from django.contrib.sitemaps.views import sitemap
from .sitemap import StaticViewSitemap, ArticleSitemap, CategorySitemap

from django.views.generic import TemplateView, RedirectView

from .views import AdsView
from articles.views import index

sitemaps = {
    'static': StaticViewSitemap,
    'categories': CategorySitemap,
    'articles': ArticleSitemap,
}

urlpatterns = [
    path('sitemap.xml', sitemap, {'sitemaps': sitemaps},
         name='django.contrib.sitemaps.views.sitemap'),
    path('robots.txt', TemplateView.as_view(
        template_name="robots.txt", content_type='text/plain')),
    path('^favicon.ico', RedirectView.as_view(
        url='/static/img/favicon.ico')),
    path('ads.txt', AdsView.as_view()),
    path('admin/', admin.site.urls),
    path('', index, name="index"),
    path('articles/', include('articles.urls', namespace='articles')),
    path('forms/', include('survey.urls', namespace='survey'))

    # path('ckeditor/', include('ckeditor_uploader.urls')),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)
