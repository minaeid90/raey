from django.contrib.sitemaps import Sitemap
from django.shortcuts import reverse
from articles.models import Category, Article


class StaticViewSitemap(Sitemap):
    def items(self):
        return ['index']

    def location(self, item):
        return reverse(item)


class CategorySitemap(Sitemap):
    def items(self):
        return Category.objects.all()

    def lastmod(self, obj):
        return obj.updated_at


class ArticleSitemap(Sitemap):
    def items(self):
        return Article.objects.all()

    def lastmod(self, obj):
        return obj.updated_at
